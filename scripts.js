/*
Roses are red,
_nounA(word1)_ are _adjectiveA(word2)_
Your beauty is that of a/n _adjectiveB(word4)_ _nounB(word3)_
*/

//noun, adjective, noun, adjective, a/n
let word1, word2, word3, word4, aOrAn, finishedPoem;

//word1 will be generated here through a starting list of plural nouns
const startingWord = ['Clothes', 'Violets', 'Snails', 'Flowers', 'Hearts', 'Souls',
'Mice', 'Teachers', 'Bunnies', 'Chocolates', 'Candies', 'Pigs', 'Spiders',
'Rubies', 'Diamonds', 'Sisters', 'Brothers', 'Cherries', 'Dogs', 'Kittens', 'Books',
'Lemons', 'Books', 'Rings', 'Jewels', 'Phones', 'Movies', 'Burgers', 'Paintings',
'Oceans', 'Mountains', 'Snakes', 'Almonds', 'Peanuts', 'Birds', 'Eagles', 'Eggs',
'Computers', 'Houses', 'Offices', 'Beds', 'Pillows', 'Sponges', 'Boots', 'Shoes',
'Pies', 'Cakes', 'Hats', 'Shirts', 'Pants', 'Bananas', 'Apples', 'Pineapples',
'Noses', 'Eyes', 'Ears', 'Hair', 'Blankets', 'Burgers', 'Fish', 'Babies', 'Oceans'];

//Part of a url string to search for adjectives
const adjQuery = 'rel_jjb=';
//url string to search for rhyming words
const rhyQuery = 'rel_rhy=';

poemWords();

//generates the four needed words for a romantic poem:
function poemWords () {
  //randomly chooses the first word from startingWord
    word1 = startingWord[Math.floor(Math.random() * startingWord.length)];
    document.getElementById('word1').innerHTML = word1;

    //callback finds words 2-4 then saves to the poemArray[]
    getWord(word1, adjQuery, 'word2', false, function (newWord){
        word2 = newWord;
        getWord(word2, rhyQuery, 'word3', true,  function (newWord){
            word3 = newWord;
            getWord(word3, adjQuery, 'word4', false, function (newWord){
                word4 = newWord;
                aOrAn = alphaWord(word4);

                document.getElementById('word4').innerHTML = word4;
                document.getElementById('alpha').innerHTML = aOrAn;
                finishedPoem = `Roses%20are%20red%2c%0a${word1}%20might%20be%20${word2}%2c%0a` +
                    `Your%20beauty%20is%20like%20${aOrAn}%20${word4}%20${word3}%2e`;

                const twitterButton = document.getElementById('twtr');
                twitterButton.setAttribute('href', 'https://twitter.com/intent/tweet?text=' + finishedPoem);
            });
        });
    });
}

//determines if 'a' or 'an'
function alphaWord (inputWord) {
    const firstChar = inputWord.charAt(0);
    return (firstChar.match(/[aeiou]/i)) ? 'an' : 'a';
}

//XML request to find adjectives and nouns
function getWord(inputWord, queryTerm, elementId, isAdjective, callback) {
    let request = new XMLHttpRequest();
    const url = 'https://api.datamuse.com/words?' + queryTerm + inputWord + '&md=d&orgin=*';
    request.open('GET', url, true);

    request.onload = function () {
    let data = JSON.parse(this.response);

    if (request.status >= 200 && request.readyState < 400) {

      //adjective lookup
        if (!isAdjective){
            //choose a random adjective from first 50% of results
            let randNum = Math.floor((Math.random() * data.length) * 0.5);
            document.getElementById(elementId).innerHTML = data[randNum].word;
            callback(data[randNum].word);

            if (typeof data[randNum] === 'undefined') {
                return poemWords();
            }
        }

      //look up rhyming words and check if a noun
        else {
            //sets the length of dictionary words to search as 4 or less
            const listLength = (data.length >= 4) ? 4 : data.length;

            for (i=0; i <= listLength;) {
                if (typeof data === 'undefined' ||
                    typeof data[i] === 'undefined' ||
                    typeof data[i].defs === 'undefined') {
                        console.log('no matching results found, starting over...');
                        return poemWords();

                //found a rhyming noun:
                } else if (data[i].defs[0].charAt(0) === 'n'){
                    document.getElementById(elementId).innerHTML = data[i].word;
                    callback(data[i].word);
                    return;

                } else {
                    console.log(data[i].word + ' is not a noun, trying next word...');
                    i++;
                    if (i >= listLength) {
                        console.log('no rhyming nouns, starting over...');
                        return poemWords();
                    }
                }
            }
        }
    }
    else {
      return console.log('status code error');
    }
  };
  // request.setRequestHeader('Accept', 'application/json');
  // request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.send();
};
