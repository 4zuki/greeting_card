# Greeting Card
Using the [Datamuse API](https://www.datamuse.com/) , I varied up a classic poem by grabbing some random nouns and adjectives that match each other to make a very entertaining poem.

To see this in action, go to [My Website](http://bone.voyage/gc/greeting_card.html)

### The poem I am generating:
>Roses are red,
>**Word1** might be **Word2**,
>Your beauty is like a/n **Word4** **Word3**

### Example of a random poem output:
>Roses are red,
>**Lemons** might be **sweet**,
>Your beauty is like a **dead** **beat**

### Parameters:
- **Word1** needs to be a plural noun
- **Word2** needs to be an adjective that describes **Word1**
- **Word3** needs to be a noun that rhymes with **Word2**
- **Word4** needs to be an adjective that describes **Word3**
- **a/n** needs to change to "a" or "an" if Word4 starts with a vowel or not

### Some of the challenges:
**Word3** needs to rhyme with **Word2** and Datamuse currently does not offer a way to find rhyming words that are also nouns. I had to add some extra code to search the definitions of each word result to find matching nouns.
